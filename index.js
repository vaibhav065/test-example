var express = require('express');
var marco = require('./marcopolo');
var number = require('./numbers');
var app = express();

app.use('/marcopolo',marco)
app.use('/digital',number)

app.listen(4000,function(){
    console.log("Port start on port 4000")
});