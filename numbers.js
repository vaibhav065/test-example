var express = require('express');
var fs = require('fs');
var router = express.Router();
module.exports = router;

router.get('/',function(req,res){
    fs.readFile('input_user.txt', 'utf8', function (err, data) {
        if (err)
            res.json(err);
        function getDigit(pattern) {
            return {
                " _ | ||_|": 0,
                "     |  |": 1,
                " _  _||_ ": 2,
                " _  _| _|": 3,
                "   |_|  |": 4,
                " _ |_  _|": 5,
                " _ |_ |_|": 6,
                " _   |  |": 7,
                " _ |_||_|": 8,
                " _ |_|  |": 9,
                " _ |_| _|": 9, // alternative 9
            }[pattern];
        }

        function getNumber(lines) {
            // Chop each line into 9 pieces of 3 chars:
            lines = lines.map(line => line.match(/.../g));
            // Combine the pieces of each digit-pattern together:
            return +lines[0].map((piece, i) => piece + lines[1][i] + lines[2][i])
                // Translate each pattern of 3x3=9 characters to a digit
                .map(getDigit)
                // Join digits together into one number
                .join('');
        }

        function pad(n, width, z) {
            z = z || '0';
            n = n + '';
            return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
        }

        const lines = data.split('\n');
        var count = 0;
        var ar = [];
        for (let i = 0; i < (lines.length / 4)-1; i++) {
            if (count == 0) {
                var a = getNumber(lines.slice(0, 3));
                ar.push(pad(a, 9))
            } else {
                var a = getNumber(lines.slice(i * 4));
                ar.push(pad(a, 9))
            }
            count++
        }
        fs.writeFile('output_user.txt', ar.join('\n'), function (err) {
            if (err) {
                return console.log(err);
            }

            res.json("The file was saved!");
        });
    })
})



