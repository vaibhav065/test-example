var express = require('express');
var router = express.Router();

router.get('/',function(req,res){
    for (let i = 1; i <= 1000000; i++) {
        if(i==1001){
            console.log('\n')
        }
        if(i%4==0 && i%7==0) {
            process.stdout.write("marcopolo ");
        }
        else if(i%4==0 ) {
            process.stdout.write("macro ");
        }
       else if(i%7==0) {
            process.stdout.write("polo ");
        }else{
            process.stdout.write(`${i} `);
        }
    }
    res.json("Done");
})

module.exports = router